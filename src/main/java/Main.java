import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        char[][] tableForRules = TableUtil.createTableForRules();
        char[][] gameTable = TableUtil.createGameTable();


        Player player1 = new Player(ScannerUtil.enterFirstPlayerName());
        player1.setIcon('X');
        Player player2 = new Player(ScannerUtil.enterSecondPlayerName());
        player2.setIcon('Y');
        MessageUtil.printPlayersIcons(player1, player2);
        MessageUtil.printRules(tableForRules);
        Player whichPlayersTurn = player1;
        Player playerWhoStartedFirst = player1;
        boolean stopTheGame = false;

        while (!stopTheGame) {
            boolean weHaveWinner = false;
            int playerChoice = 0;
            List<Integer> usedChoices = new ArrayList<>();
            gameTable = TableUtil.createGameTable();


            while (!weHaveWinner) {
                boolean choiceAlreadyWasMade = true;
                while (choiceAlreadyWasMade) {
                    MessageUtil.printPlayerHasToChooseMessage(whichPlayersTurn);
                    playerChoice = ScannerUtil.playerChoiceWithSimpleValidator();
                    choiceAlreadyWasMade = TableUtil.checkIfChoiceAlreadyWasMade(usedChoices, playerChoice);
                    if (choiceAlreadyWasMade) {
                        MessageUtil.printChoiceWasUsedMessage();
                    }
                }
                usedChoices.add(playerChoice);
                TableUtil.updateTable(gameTable, whichPlayersTurn, playerChoice);
                MessageUtil.printTable(gameTable);
                weHaveWinner = TableUtil.checkDoWeHaveWinner(gameTable);
                if (weHaveWinner) {
                    MessageUtil.printWinnerMessage(whichPlayersTurn);
                    whichPlayersTurn.setScore(whichPlayersTurn.getScore() + 1);
                }
                whichPlayersTurn = whichPlayersTurn == player1 ? player2 : player1;
            }
            MessageUtil.printScores(player1, player2);
            MessageUtil.doYouWantRestartTheGame();
            stopTheGame = !ScannerUtil.returnYOrN();
            whichPlayersTurn = playerWhoStartedFirst == player1 ? player2 : player1;
            playerWhoStartedFirst = whichPlayersTurn;

        }
        System.out.println("--EXIT--");
    }
}

