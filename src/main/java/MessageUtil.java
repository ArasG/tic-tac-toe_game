public class MessageUtil {

    public static void printPlayersIcons(Player player1, Player player2) {
        System.out.println("\nZaideju simboliai:");
        System.out.println(player1.getName() + " - " + player1.getIcon());
        System.out.println(player2.getName() + " - " + player2.getIcon());
    }

    public static void printRules(char[][] table) {
        System.out.println("\nTAISYKLES: Zaidejas turi pasirinkti skaiciu nuo 1 iki 9;");
        printTable(table);
        System.out.println("Pradedam zaidima!\n");
    }

    public static void printTable(char[][] table) {
        for (char[] block : table) {
            for (char simbol : block) {
                System.out.print(simbol);
            }
            System.out.println();
        }
    }

    public static void printPlayerHasToChooseMessage(Player player) {
        System.out.println("\n" + player.getName() + " renkasi langeli:");
    }

    public static void printChoiceWasUsedMessage() {
        System.out.println("Toks pasirinkimas jau buvo. Bandykite dar karta.");
    }

    public static void printWinnerMessage(Player player) {
        System.out.println("TURIM LAIMETOJA!\nZaidima laimejo " + player.getName());
    }

    public static void doYouWantRestartTheGame() {
        System.out.println("Ar norite kartoti? Y - taip / N - exit;");
    }

    public static void printScores(Player player1, Player player2){
        System.out.println("\nVISU ZAIDIMU REZULTAS:");
        System.out.println(player1.getName() + " : " + player2.getName());
        System.out.println(player1.getScore() + " : " + player2.getScore());
    }

}
