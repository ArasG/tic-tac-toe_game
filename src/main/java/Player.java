public class Player {

    private String name;
    private int score;
    private char icon;

    public  Player (String name){
        this.name = name;
        this.score = 0;
    }

    public void setIcon(char icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public char getIcon() {
        return icon;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
