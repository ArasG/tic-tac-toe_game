import java.util.InputMismatchException;
import java.util.Scanner;

public class ScannerUtil {

    public static final Scanner SCANNER = new Scanner(System.in);

    public static String enterFirstPlayerName() {
        System.out.print("-Iverkite pirmo zaidejo varda: ");
        return SCANNER.nextLine();
    }

    public static String enterSecondPlayerName() {
        System.out.print("-Iverkite antro zaidejo varda: ");
        return SCANNER.nextLine();
    }

    public static int playerChoice() { // TODO check why SCANNER doesnt retrun after catch block;
        int result = 99;
        boolean inputCorrect = false;
        while (!inputCorrect) {
            try {
                result = SCANNER.nextInt();
                System.out.println("result: " + result);
                if (result != 99) {
                    inputCorrect = true;
                    System.out.println("if");
                }
            } catch (InputMismatchException e) {
                System.out.println("KLAIDA: IVESTAS NE SKAICIUS. BANDYKITE DAR KARTA");
                System.out.println("result: " + result);
                // result = SCANNER.nextInt();
                break;
            }
        }
        return result;
    }

    public static int playerChoiceWithSimpleValidator() {
        char choice = ' ';
        boolean inputIsDigit = false;
        while (!inputIsDigit) {
            choice = SCANNER.next().charAt(0);
            if (Character.isDigit(choice)) {
                inputIsDigit = true;
            } else {
                System.out.println("KLAIDA: IVESTAS NE SKAICIUS. BANDYKITE DAR KARTA");
            }
        }
        return Integer.parseInt(String.valueOf(choice));
    }

    public static boolean returnYOrN() {
        String result = " ";
        while (!result.equals("y") && !result.equals("n")) {
            result = SCANNER.next().toLowerCase();
            if (!result.equals("y") && !result.equals("n")) {
                System.out.println("Iveskit Y arba N");
            }
        }
        if (result.equals("y")) {
            return true;
        } else {
            return false;
        }
    }


}
