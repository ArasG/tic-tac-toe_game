import java.util.List;

public class TableUtil {

    public static char[][] createTableForRules() {
        char[][] table = {
                {'1', '|', '2', '|', '3'},
                {'4', '|', '5', '|', '6'},
                {'7', '|', '8', '|', '9'}};
        return table;
    }

    public static char[][] createGameTable() {
        char[][] table = {
                {' ', '|', ' ', '|', ' '},
                {' ', '|', ' ', '|', ' '},
                {' ', '|', ' ', '|', ' '}};
        return table;
    }

    public static char[][] updateTable(char[][] table, Player player, int playerChoice) {
        switch (playerChoice) {
            case 1:
                table[0][0] = player.getIcon();
                break;
            case 2:
                table[0][2] = player.getIcon();
                break;
            case 3:
                table[0][4] = player.getIcon();
                break;
            case 4:
                table[1][0] = player.getIcon();
                break;
            case 5:
                table[1][2] = player.getIcon();
                break;
            case 6:
                table[1][4] = player.getIcon();
                break;
            case 7:
                table[2][0] = player.getIcon();
                break;
            case 8:
                table[2][2] = player.getIcon();
                break;
            case 9:
                table[2][4] = player.getIcon();
                break;
        }
        return table;
    }

    public static boolean checkIfChoiceAlreadyWasMade(List<Integer> choices, int choice) {
        return choices.contains(choice);
    }

    public static boolean checkDoWeHaveWinner(char[][] gameTable) {
        if (gameTable[0][0] == gameTable[0][2] && gameTable[0][0] == gameTable[0][4] && gameTable[0][0] != ' ') {
            return true;
        }
        if (gameTable[1][0] == gameTable[1][2] && gameTable[1][0] == gameTable[1][4] && gameTable[1][0] != ' ') {
            return true;
        }
        if (gameTable[2][0] == gameTable[2][2] && gameTable[2][0] == gameTable[2][4] && gameTable[2][0] != ' ') {
            return true;
        }
        if (gameTable[0][0] == gameTable[1][0] && gameTable[0][0] == gameTable[2][0] && gameTable[0][0] != ' ') {
            return true;
        }
        if (gameTable[0][2] == gameTable[1][2] && gameTable[0][2] == gameTable[2][2] && gameTable[0][2] != ' ') {
            return true;
        }
        if (gameTable[0][4] == gameTable[1][4] && gameTable[0][4] == gameTable[2][4] && gameTable[0][4] != ' ') {
            return true;
        }
        if (gameTable[2][0] == gameTable[1][2] && gameTable[2][0] == gameTable[0][4] && gameTable[2][0] != ' ') {
            return true;
        }
        if (gameTable[0][0] == gameTable[1][2] && gameTable[0][0] == gameTable[2][4] && gameTable[0][0] != ' ') {
            return true;
        }
        return false;
    }
}
